#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#ifndef _OPENMP
#error "OpenMP is part of this sanity check. Recompile with OpenMP."
#endif
#include <omp.h>
#define __USE_GNU
#include <sched.h>
#include <stdint.h>

#define BITMASKSIZE (CPU_SETSIZE/32)
#define CPU_PRINT_LIMIT 128

void get_affinity(uint32_t ret[BITMASKSIZE])
{
  cpu_set_t cpuset;
  if(sched_getaffinity(0, sizeof(cpu_set_t), &cpuset) == -1) {
    perror("sched_getaffinity");
  }
  for(int i = 0; i < BITMASKSIZE; ++i) {
    ret[i] = 0;
  }
  for(int i = 0; i < CPU_SETSIZE; ++i) {
    if(CPU_ISSET(i, &cpuset)) {
      ret[i / 32] |= ((uint32_t)1) << (i % 32);
    }
  }
}

void get_affinity_string(char* buffer)
{
  uint32_t mask[BITMASKSIZE];
  get_affinity(mask);
  for(int i = 0; i < (CPU_PRINT_LIMIT / 32); ++i) {
    // FIXME: assumes 32bit is "X", but what should be ? 
    sprintf(buffer + i * 8, "%08X", mask[(CPU_PRINT_LIMIT / 32) - 1 - i]);
  }
}

int main(int argc, char* argv[])
{
  int size;
  int rank;
  char hostname[MPI_MAX_PROCESSOR_NAME];
  int hostname_len;

  /* I know this is bad */
  const int BSIZE = 1000000;
  volatile char* buf = calloc(BSIZE, sizeof(char));
  volatile int bufptr = 0;

  char *recvbuf = calloc(BSIZE, sizeof(char));
  volatile char affinitybuf[BITMASKSIZE * 8 + 1];

  int nthread;

  MPI_Status st;

#pragma omp parallel
  {
    nthread = omp_get_num_threads();
  }

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  for(int i = 0; i < size; ++i) {
    if(rank == i) {
      MPI_Get_processor_name(hostname, &hostname_len);
      get_affinity_string((char*)affinitybuf);
      sprintf((char*)buf,
	      "Rank %d: Host %s, proc mask %s, nthread %d\n",
	      rank, hostname, affinitybuf, nthread);

      for(int tprint = 0; tprint < nthread; ++tprint) {
#pragma omp parallel for default(none) schedule(static, 1) shared(nthread, affinitybuf, buf, tprint)
	for(int thread = 0; thread < nthread; ++thread) {
	  if(tprint == thread) {
	    get_affinity_string((char*)affinitybuf);
	    sprintf((char*)buf + strlen((char*)buf),
		    "  thread %d: %s\n",
		    omp_get_thread_num(), affinitybuf);
	  }
	}
      }
      if(rank == 0) {
	strcpy(recvbuf, (char*)buf);
      }else{
	MPI_Send((char*)buf, BSIZE, MPI_CHARACTER, 0, 0, MPI_COMM_WORLD);
      }
    }else if(rank == 0){
      MPI_Recv(recvbuf, BSIZE, MPI_CHARACTER, i, 0, MPI_COMM_WORLD, &st);
    }
    if(rank == 0){
      printf("%s", recvbuf);
    }
  }

  // Finalize the MPI environment.
  MPI_Finalize();
}

